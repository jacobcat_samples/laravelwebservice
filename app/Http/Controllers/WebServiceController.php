<?php
/**
 * Created by IntelliJ IDEA.
 * User: ultim
 * Date: 04/02/2016
 * Time: 4:36 AM
 */

namespace App\Http\Controllers;


use App\Http\Requests\Request;

class WebServiceController extends Controller
{
    // sample URL: http://localhost:8000/ws/helloworld
    // sample output: { "message" : "Hello world." }
    function showHelloWorld() {
        $result = ['message' => 'Hello world.'];
        return response()->json($result);
    }

    // sample URL: http://localhost:8000/ws/getsample/3
    // sample output: { "status" : "ok", "id" : "3" }
    function sampleGetMethod($id) {
        $result = ['status' => 'ok', 'id' => $id];
        return response()->json($result);
    }

    // sample URL: http://localhost:8000/ws/postsample
    // sample POST payload: { "id" : "3", "name" : "Jake" }
    // sample output: { "status" : "ok", "id" : "3", "name" : "Jake" }
    function samplePostMethod() {
        if(request()->isJson()) {
            $input = request()->json();
            $result = [ 'status' => 'ok' ];
            foreach($input as $key => $val) {
                $result["$key"] = $val;
            }
        }
        else {
            $result = [ 'status' => 'error', 'message' => 'POST data is not in JSON format.' ];
        }
        return response()->json($result);
    }
}